import hydra
import pandas as pd
from hydra.utils import instantiate
from hydra import compose, initialize
from omegaconf import OmegaConf, DictConfig
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
from lightgbm import LGBMRegressor
from catboost import CatBoostRegressor
import logging
log = logging.getLogger(__name__)

#интеграция чтения конфигурации через compose-API
# initialize(version_base=None, config_path="conf")
# cfg = compose(config_name="config")

@hydra.main(version_base=None, config_path='conf', config_name='config')
def my_app(cfg: DictConfig):
   
    # Загружаем данные
    try:
        df = pd.read_csv('data/house-prices.csv')
    except FileNotFoundError:
        log.error("Файл данных не найден")
        return
    features = ['SqFt','Bedrooms','Bathrooms','Offers']

    y = df.Price
    X = df[features]

    # Формируем трейн/тест
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=cfg['preprocessing']['test_size'],random_state=10)
    
    # Обучение модели
    model = instantiate(cfg['model'])
    model.fit(X_train, y_train)
      
    # Оценка
    y_pred = model.predict(X_test)
    mse = round(mean_squared_error(y_test, y_pred), 2)
    log.info(f'MSE: {mse}')
    
    #dump(model, "models/model")

if __name__ == "__main__":
    my_app()