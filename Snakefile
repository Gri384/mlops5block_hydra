from snakemake.utils import report
import os

preprocessing_methods = ["method1", "method2"]
model_types = ["type1", "type2"]

rule all:
    input:
        expand("models/model_pre_{preprocessing_method}_train_{model_type}.joblib", preprocessing_method=preprocessing_methods, model_type=model_types)
# Правило для чтения данных
rule read_data:
    input:
        "data/house-prices.csv"
    output:
        "data/data.csv"
    shell:
        "cat {input} > {output}"

# Правило для предобработки данных
rule preprocess_data:
    input:
        "data/data.csv"
    output:
        "data/preprocessed_data_{preprocessing_method}.csv"
    script:
        "scripts/preprocess_{wildcards.preprocessing_method}.py"

# Правило для обучения модели
rule train_model:
    input:
        "data/preprocessed_data_{preprocessing_method}.csv"
    output:
        "models/model_pre_{preprocessing_method}_train_{model_type}.joblib"
    threads: int((len(os.sched_getaffinity(0)) + 1) / 2)
    script:
        "scripts/train_model_{wildcards.model_type}.py"
